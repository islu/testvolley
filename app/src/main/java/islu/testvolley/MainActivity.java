package islu.testvolley;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {
    EditText id;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        id = (EditText) findViewById(R.id.editTextId);
        id.setText("");

        Button btn = (Button) findViewById(R.id.button);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String codi = id.getText().toString();
                if((codi.isEmpty()) || (Integer.parseInt(codi) <= 0) || (Integer.parseInt(codi)) > 100)
                    codi = "1";

                Intent intent = new Intent(v.getContext(), Main2Activity.class);
                intent.putExtra("postID",codi);
                startActivity(intent);

            }
        });
    }


}
