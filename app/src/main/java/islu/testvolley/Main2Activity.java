package islu.testvolley;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;


import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

public class Main2Activity extends AppCompatActivity {
    public TextView id, userId, title, body;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        Bundle bundle = getIntent().getExtras();
        String post = bundle.getString("postID");

        String url = "https://jsonplaceholder.typicode.com/posts/";
        id = (TextView) findViewById(R.id.textViewId);
        userId = (TextView) findViewById(R.id.textViewuserId);
        title = (TextView) findViewById(R.id.textViewTitle);
        body = (TextView) findViewById(R.id.textViewBody);


        // Instantiate the RequestQueue
        RequestQueue requestQueue = Volley.newRequestQueue(this);

        //Prepare the Request
        JsonObjectRequest request = new JsonObjectRequest(
                Request.Method.GET, //GET or POST
                url+post, //URL
                null, //Parameters
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) { //Listener OK
                        Log.d("respuesta object OK: ",response.toString());

                        try {
                            id.setText(response.getString("id"));
                            userId.setText(response.getString("userId"));
                            title.setText(response.getString("title"));
                            body.setText(response.getString("body"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    }, new Response.ErrorListener() { //Listener ERROR

            @Override
            public void onErrorResponse(VolleyError error) {
                //There was an error :(
                Log.d("rspuesta ERROR",error.toString());
            }
        });

        //Send the request to the requestQueue
        requestQueue.add(request);


        Button home = (Button) findViewById(R.id.buttonHome);

        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), MainActivity.class);
                startActivity(intent);
            }
        });
    }
}
